#include "gpu_radix_supersort.h"
#include <cstdio>
#include <memory>
#define TILE_SIZE (256)

#ifdef DEBUG_MODE
#define RECORD_EVENT(event) cudaEventRecord(event)
#define PRINT_TIME(start, end)						\
	do {								\
		float ms;						\
		cudaEventElapsedTime(&ms, start, end);			\
		printf("Time between %30s to %30s was %10.2f ms\n", #start, #end, ms); \
	} while (0)
#define SYNC_EVENT(event) cudaEventSynchronize(event)
#define PRINT_DELIMITER() printf("*********************************************************\n");
#define CREATE_EVENT(event) cudaEvent_t event; cudaEventCreate(&event)
#define DESTROY_EVENT(event) cudaEventDestroy(event);
#else
#define RECORD_EVENT(event)
#define PRINT_TIME(start, end)
#define SYNC_EVENT(event)
#define PRINT_DELIMITER()
#define CREATE_EVENT(event)
#define DESTROY_EVENT(event)
#endif

/* Counts a histogram from all the values in array and enters into hist. */
__device__ void histogram(int size, int pow, int* hist, int* items, 
			  int* lower_end, int* upper_end) {
	const int idx = threadIdx.x;
	const int mine = (items[idx] >> pow) & 0x000f;

	items[idx] = mine;
	lower_end[threadIdx.x] = upper_end[threadIdx.x] = -1;
	__syncthreads();
	
	// Find the upper and lower bounds to all 1 << 4 combinations
	// at max 16 threads will write to upper_end/lower_end arrays,
	// and no threads will overlap (since only one item can be at
	// the end of sorted arrays. We write the end similar to C/C++
	// arrays where the array is a range [a, b) **
	if (idx == 0 || items[idx - 1] != items[idx])
		lower_end[mine] = idx; 

	if (threadIdx.x == TILE_SIZE - 1 || items[idx + 1] != items[idx])
		upper_end[mine] = idx + 1; // ** which is why there is +1 here.

	// Write local histogram to global. Let the 16 first threads
	// use their index to find the count for their bitcount.
	__syncthreads();
	if (idx < 16) {
		// In case an element is not part of the sequence the
		// equation will be (-1) - (-1) == 0 Which yields a
		// count of 0, which is correct.
		const int count = upper_end[idx] - lower_end[idx];
		const int pos = gridDim.x * idx + blockIdx.x;
		hist[pos] = count;
	}
}


/** Sorts a single tile on 4 bits starting from the pow'th bit */
__global__ void tile_sort(int* array, int size, int pow, int* hist) {
	__shared__ int tile[TILE_SIZE];
	__shared__ int zeroes[TILE_SIZE];
	__shared__ int ones[TILE_SIZE];
	__shared__ int twos[TILE_SIZE];
	__shared__ int threes[TILE_SIZE];

	// Read tile to shared memory
	const int idx = threadIdx.x;
	const int arr_idx = idx + (blockDim.x)*blockIdx.x;
	tile[idx] = array[arr_idx];

	// Do several 1-bit splits, each reordering the sequence by a
	// lot In each split we compute two arrays, each array is the
	// same size as the tile to sort The two arrays are opposites
	// of each other. If there is a one in either the other has a
	// zero This is determined by the current bit to split on.
	// After computing this we run prefix sum on both
	// (individually) and add the last value to the ones array.
	// We have now computed the rank for each element for the
	// current 1-bit split.
	for (int split = 0; split < 4; split += 2) {
		__syncthreads();
		const int mine = tile[idx];
		const int bit = 3 << (pow + split);
		const int bottom = (mine & bit) >> (pow + split);
		zeroes[idx] = bottom == 0;
		ones[idx] = bottom == 1;
		twos[idx] = bottom == 2;
		threes[idx] = bottom == 3;
		
		// Prefix sum over zeroes/ones. We need to split it
		// into a read-phase and a write-phase.
		for (int stride = 1; stride < TILE_SIZE; stride *= 2) {
			__syncthreads();
			int zero_add = 0;
			int ones_add = 0;
			int two_add = 0;
			int three_add = 0;
			if (idx >= stride) {
				zero_add = zeroes[idx - stride];
				ones_add = ones[idx - stride];
				two_add = twos[idx - stride];
				three_add = threes[idx - stride];
			}
			__syncthreads();
			zeroes[idx] += zero_add;
			ones[idx] += ones_add;
			twos[idx] += two_add;
			threes[idx] += three_add;
		}

		// Write result to correct position in the array. To
		// quickly compute the histogram we use the last
		// element of previous arrays, depending on the way
		// our bits were set.
		__syncthreads();
		if (bottom == 0) {
			tile[zeroes[idx] - 1] = mine;
		} else if (bottom == 1) {
			tile[ones[idx] + zeroes[TILE_SIZE - 1] - 1] = mine;
		} else if (bottom == 2) {
			tile[twos[idx] + ones[TILE_SIZE - 1] + zeroes[TILE_SIZE - 1] - 1] = mine;
		} else if (bottom == 3) {
			tile[threes[idx] + twos[TILE_SIZE - 1] + ones[TILE_SIZE - 1] + zeroes[TILE_SIZE - 1] - 1] = mine;
		}

	}

	__syncthreads();
	array[arr_idx] = tile[idx]; // Write back tile to global array
	__syncthreads();
	histogram(size, pow, hist, tile, ones, twos); // Reuse shared memory as upper and lower ends
}

__global__ void parallel_prefix_sum(int* array, int* out, int size, int stride) {
	// Standard parallel prefix sum
	const int arr_idx = threadIdx.x + (blockDim.x)*blockIdx.x;
	if (arr_idx >= stride && arr_idx < size) {
		out[arr_idx] = array[arr_idx] + array[arr_idx - stride];
	} else if (arr_idx < size) {
		out[arr_idx] = array[arr_idx];
	}
}

__global__ void write_tile(int* inarray, int* outarray, int size,
			   int* prefix_sum, int pow) {
	__shared__ int items[TILE_SIZE];
	__shared__ int lower_end[16];

	// Write current bitmask to shared memory.
	const int idx = threadIdx.x;
	const int tile = blockIdx.x;
	const int x = inarray[idx + tile * blockDim.x];
	const int bitmask = (x >> pow) & 0x000f;
	items[idx] = bitmask;
	__syncthreads();
	
	// Find the lower end to all available elements in the tile.
	if (idx == 0 || items[idx - 1] != bitmask) {
		lower_end[bitmask] = idx;
	}
	__syncthreads();
	
	// Write this tile to global memory, using the calculated
	// prefix sum so that the numbers of the tile are placed in a
	// sorted order. (for the current 4 bits).
	const int pos = (bitmask * gridDim.x) + tile - 1;
	const int psum = (pos >= 0) ? prefix_sum[pos] : 0;
	const int item_offset = psum + (threadIdx.x - lower_end[bitmask]);
	outarray[item_offset] = x;
}

void gpu_radix_sort(int* array, int count) {
	const int num_blocks = count / TILE_SIZE;
	const int p = num_blocks;
	const int hist_count = 16 * p; // We always use 4 bits so we have 1 << 4 combinations of bits.
	dim3 blockDim(TILE_SIZE, 1);
	dim3 gridDim(p, 1);

	int* to_sort;
	int* buffer;
	int* hist;
	cudaMalloc((void**) &to_sort, sizeof(int) * count);
	cudaMalloc((void**) &buffer, sizeof(int) * count);
	cudaMalloc((void**) &hist, sizeof(int) * hist_count);

	CREATE_EVENT(tile_sort_start);
	CREATE_EVENT(tile_sort_stop);
	CREATE_EVENT(parallel_prefix_start);
	CREATE_EVENT(parallel_prefix_stop);
	CREATE_EVENT(write_tile_start);
	CREATE_EVENT(write_tile_stop);

	cudaMemcpy(to_sort, array, sizeof(int) * count, cudaMemcpyHostToDevice);
	for (int pow = 0; pow < 32; pow += 4) {
	        PRINT_DELIMITER();
		cudaMemset(hist, 0, sizeof(int) * hist_count);

		// Run tile sorting.
		RECORD_EVENT(tile_sort_start);
		tile_sort<<< gridDim, blockDim >>>(to_sort, count, pow, hist);
		RECORD_EVENT(tile_sort_stop);
		cudaThreadSynchronize();

		// Calculate prefix sum of global histogram
		// Used in order to get exclusive sum
		RECORD_EVENT(parallel_prefix_start);
		bool into_buffer = true;
		for (int stride = 1; stride < hist_count; stride *= 2) {
			if (into_buffer)
				parallel_prefix_sum<<<gridDim, blockDim>>>(hist, buffer, hist_count, stride);
			else
				parallel_prefix_sum<<<gridDim, blockDim>>>(buffer, hist, hist_count, stride);
			cudaThreadSynchronize();
			into_buffer = !into_buffer;
		}
		if (!into_buffer) cudaMemcpy(hist, buffer, sizeof(int) * hist_count, cudaMemcpyDeviceToDevice);
		RECORD_EVENT(parallel_prefix_stop);
		
		// Write tiles
		RECORD_EVENT(write_tile_start);
		write_tile<<< gridDim, blockDim >>>(to_sort, buffer, count, hist, pow);
		RECORD_EVENT(write_tile_stop);
		cudaThreadSynchronize();
		cudaMemcpy(to_sort, buffer, sizeof(int) * count, cudaMemcpyDeviceToDevice);
		//std::swap(to_sort, buffer); // Swap the buffers so that to_sort holds the sorted array.
		// Sync last event and print events
		SYNC_EVENT(write_tile_stop);
		PRINT_TIME(tile_sort_start, tile_sort_stop);
		PRINT_TIME(parallel_prefix_start, parallel_prefix_stop);
		PRINT_TIME(write_tile_start, write_tile_stop);
	}

	// Copy result and free memory
	cudaMemcpy(array, to_sort, sizeof(int) * count, cudaMemcpyDeviceToHost);
	cudaFree(to_sort);
	cudaFree(hist);
	cudaFree(buffer);

	DESTROY_EVENT(tile_sort_start);
	DESTROY_EVENT(tile_sort_stop);
	DESTROY_EVENT(parallel_prefix_start);
	DESTROY_EVENT(parallel_prefix_stop);
	DESTROY_EVENT(write_tile_start);
	DESTROY_EVENT(write_tile_stop);
}
