#include <omp.h>
#include <climits>
#include <cmath>
#include <algorithm>
#include <numeric>

#include "sampleSort.hpp"

const int NUM_THREADS = 6;

const int SAMPLES_PER_THREAD = 40;
const int NUM_SAMPLES = NUM_THREADS * SAMPLES_PER_THREAD;


inline void multimerge(int *starts[NUM_THREADS], int lengths[NUM_THREADS], int outputArray[]) {	
	// Naive implementation of multimerge, suitable for low number of input arrays. Multimerge using heap proved much slower.
	size_t totalAmount = std::accumulate(lengths, lengths + NUM_THREADS, 0);
	
	for(size_t outIndex = 0; outIndex < totalAmount; outIndex++) {
		size_t smallestIndex = 0;
		int smallestValue = INT_MAX;
		// Find the input array with the smallest first element
		for(size_t i = 0; i < NUM_THREADS; i++) {
			if(lengths[i] > 0 and starts[i][0] < smallestValue) {
				smallestValue = starts[i][0];
				smallestIndex = i;
			}
		}
		
		// Copy smallest element to output array
		outputArray[outIndex] = starts[smallestIndex][0];
		starts[smallestIndex]++;
		lengths[smallestIndex]--;
	}
}


// Lightning fast super sorting algorithm using splitters and comparisons and threads and magic!
// According to http://parallelcomp.uw.hu/ch09lev1sec5.html
// and http://csweb.cs.wfu.edu/bigiron/LittleFE-PSRS/build/html/PSRSalgorithm.html
void sampleSort(int* array, const size_t length) {
	int** buckets = new int*[NUM_THREADS];
	
	int samples[NUM_SAMPLES];
	int splitters[NUM_THREADS-1];
	
	// To find out if array is constant
	int firstElem = array[0];
	bool isConstant[NUM_THREADS];

	// First and last element from each bucket after they been sorted
	int borderElements[NUM_THREADS*2];
	
#pragma omp parallel num_threads(NUM_THREADS)
	{
		// Sort the buckets in parallel
		const size_t threadId = omp_get_thread_num();
		size_t work = length/NUM_THREADS;
		const size_t globalOffset = threadId*work;
		if(threadId == NUM_THREADS-1)
			work += (length - work*NUM_THREADS); // Let last thread take care of reminder of array
		
		buckets[threadId] = new int[work];
			
		std::copy(array + globalOffset, array + globalOffset+work, buckets[threadId]);
		std::sort(buckets[threadId], buckets[threadId]+work);
		
		isConstant[threadId] = (firstElem == buckets[threadId][0] and buckets[threadId][0] == buckets[threadId][work-1]);
		borderElements[threadId*2] = buckets[threadId][0];
		borderElements[threadId*2+1] = buckets[threadId][work-1];
		
		// Pick samples
		const size_t jumps = round(work/(SAMPLES_PER_THREAD+1.0));
		for(int i = 0; i < SAMPLES_PER_THREAD; i++)
			samples[threadId*SAMPLES_PER_THREAD + i] = buckets[threadId][i*jumps + (jumps-1)];
	} // end parallel block
	

	// Check if constant array
	if( std::all_of(isConstant, isConstant + NUM_THREADS, [](bool b){ return b; }) ) {
		for(size_t i = 0; i < NUM_THREADS; i++)
			delete[] buckets[i];
		delete[] buckets;
		return;
	}
		
	// Check if already sorted
	bool isSorted = true;
	for(size_t i = 0; i < NUM_THREADS*2-1; i++) {
		if(borderElements[i] > borderElements[i+1])
			isSorted = false;
	}
	if(isSorted) {
#pragma omp parallel num_threads(NUM_THREADS)
		{
			// Copy back the sorted buckets
			const size_t threadId = omp_get_thread_num();
			size_t work = length/NUM_THREADS;
			const size_t globalOffset = threadId*work;
			if(threadId == NUM_THREADS-1)
				work += (length - work*NUM_THREADS); // Let last thread take care of reminder of array
			std::copy(buckets[threadId], buckets[threadId] + work, array + globalOffset);
			delete[] buckets[threadId];
		} // End parallel block
		
		delete[] buckets;
		return;
	}
	
	// Sort the samples and pick some splitters
	std::sort(samples, samples + NUM_SAMPLES);
	int distBetweenSamples = NUM_SAMPLES/NUM_THREADS;
	for(int i = 0; i < NUM_THREADS-1; i++)
		splitters[i] = samples[(i+1)*distBetweenSamples];
	
	// Pointers to the splitters in the buckets.
	// First and last indecies in each row are the first and past last index of the bucket, 
	// the rest are pinters to the element above the spitter in the buckets
	int* partitionIndecies[NUM_THREADS][NUM_THREADS+1];
	// 1D array of the partition sizes in each bucket. AccumSum from begining to 
	// threadId*NUM_THREADS gives the writeback start address in the global array
	size_t partitionSizes[NUM_THREADS*NUM_THREADS];
	
#pragma omp parallel num_threads(NUM_THREADS)
	{
		// Find the splitters in each partition and write the indecies to the partition array
		const size_t threadId = omp_get_thread_num();
		size_t work = length/NUM_THREADS;
		if(threadId == NUM_THREADS-1)
			work += (length - work*NUM_THREADS); // Let last thread take care of reminder of array
		
		// Start and end pointers for the bucket
		partitionIndecies[threadId][0] = buckets[threadId];
		partitionIndecies[threadId][NUM_THREADS] = buckets[threadId] + work;
		
		// Find the splitters inside this threads sorted partition in O( log(n/p) ) time
		for(size_t i = 0; i < NUM_THREADS-1; i++) {
			int* splitterPtr = std::upper_bound(buckets[threadId], buckets[threadId] + work, splitters[i]);
			partitionIndecies[threadId][i+1] = splitterPtr;
			partitionSizes[i*NUM_THREADS + threadId] = splitterPtr - partitionIndecies[threadId][i];
		}
		// Add size of last partition
		partitionSizes[NUM_THREADS*(NUM_THREADS-1) + threadId] = partitionIndecies[threadId][NUM_THREADS-1] - partitionIndecies[threadId][NUM_THREADS];
		
#pragma omp barrier
		// Parallel multi-merge using NUM_THREADS where each thread merges the i:th partitions of the NUM_THREADS buckets
		
		// How far into the result array we should merge the code
		const size_t globalMergeOffset = std::accumulate(partitionSizes, partitionSizes + threadId*NUM_THREADS, 0);
		
		// Find partitions of the input arrays to merge
		int* starts[NUM_THREADS];
		int lengths[NUM_THREADS];
		
		for(size_t i = 0; i < NUM_THREADS; i++) {
			starts[i] = partitionIndecies[i][0+threadId];
			lengths[i] = partitionIndecies[i][1+threadId] - starts[i];
		}
		
		multimerge(starts, lengths, array + globalMergeOffset);
		
	} // end parallel block
	
	for(size_t i = 0; i < NUM_THREADS; i++)
		delete[] buckets[i];
	delete[] buckets;
}

