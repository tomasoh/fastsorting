# FastSorting

During the autumn of 2016 I read the course TDDD56 - Multicore and
GPU-programming together with Henrik Adolfsson. The course had an optional part where you
had to construct and compete with the fastest CPU and GPU sorting code
you could create. This repo are the two resulting programs that we
wrote. We even managed to win both categories with it!

The CPU implementation can be found in sampleSort.cpp and is a "sample
sort", explained more in detail in the file.

The GPU implementation can be found in gpu_radix_supersort.cu and is a
"radix sort", explained more in detail in the file.
